﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SingingEasy
{
    class SingingEasy
    {
        public static void Main(string[] args)
        {


            int[] pitch = { 1,0,0,0,0,0,0 };

            if (pitch.Length % 5 == 0)
            {
                int getHalf_unsorted = getHalf(pitch);
                int getABBAA_unsorted = getABBAA(pitch);
                int firstwithrest_unsorted = firswithrest(pitch);
                int allwithlast_unsorted = allwithlast(pitch);
                int[] pitch_sorted = pitch;
                Array.Sort(pitch_sorted);
                int getHalf_sorted = getHalf(pitch_sorted);
                int getABBAA_sorted = getABBAA(pitch_sorted);
                int firstwithrest_sorted = firswithrest(pitch_sorted);
                int allwithlast_sorted = allwithlast(pitch_sorted);

                solve(getABBAA_unsorted, getABBAA_sorted, getHalf_unsorted, getHalf_sorted, firstwithrest_unsorted, firstwithrest_sorted, allwithlast_unsorted, allwithlast_sorted);
            }

            else
            {
                int getHalf_unsorted = getHalf(pitch);
                int firstwithrest_unsorted = firswithrest(pitch);
                int allwithlast_unsorted = allwithlast(pitch);
                int[] pitch_sorted = pitch;
                Array.Sort(pitch_sorted);
                int getHalf_sorted = getHalf(pitch_sorted);
                int firstwithrest_sorted = firswithrest(pitch_sorted);
                int allwithlast_sorted = allwithlast(pitch_sorted);



                getResult(getHalf_unsorted, getHalf_sorted, firstwithrest_unsorted, firstwithrest_sorted, allwithlast_unsorted, allwithlast_sorted);



            }



        }


        public static int getHalf(int[] pitch)
        {

            int gjatsia = pitch.Length;
            int bob;
            int alice;
            int Alice_shuma = 0;
            int Bob_shuma = 0;






            if (gjatsia % 2 == 0)
            {
                alice = gjatsia / 2;
                bob = alice;




                for (int i = 0; i < alice - 1; i++)
                {

                    Alice_shuma += Math.Abs(pitch[i + 1] - pitch[i]);

                }

                for (int i = alice; i < alice + bob - 1; i++)
                {

                    Bob_shuma += Math.Abs(pitch[i + 1] - pitch[i]);

                }

                //Console.Write("Shuma GetHalf: " + Bob_shuma + Alice_shuma);

                return (Bob_shuma + Alice_shuma);

            }

            else
            {
                alice = gjatsia / 2;
                bob = alice + 1;

                for (int i = 0; i < alice - 1; i++)
                {

                    Alice_shuma += Math.Abs(pitch[i + 1] - pitch[i]);

                }

                for (int i = alice; i < alice + bob - 1; i++)
                {

                    Bob_shuma += Math.Abs(pitch[i + 1] - pitch[i]);

                }

                // Console.Write("Shuma GetHalf: " + Bob_shuma + Alice_shuma);

                return Bob_shuma + Alice_shuma;



            }






        }


        public static int getABBAA(int[] pitch)
        {

            int shuma_A = 0, shuma_B = 0, rez1 = 0, rez2 = 0;


            List<int> alice = new List<int>();
            List<int> bob = new List<int>();

            for (int i = 0; i < pitch.Length; i = i + 5)
            {

                for (int j = i; j < i + 1; j++)
                {

                    rez1 = Math.Abs(pitch[j + 3] - pitch[j]) + Math.Abs(pitch[j + 4] - pitch[j + 3]);
                    alice.Add(rez1);



                }
                for (int k = i + 1; k < i + 2; k++)
                {
                    rez2 = Math.Abs(pitch[k + 1] - pitch[k]);
                    bob.Add(rez2);
                }
            }

            foreach (int vlera in alice)
            {
                shuma_A += vlera;
            }

            foreach (int vlera in bob)
            {
                shuma_B += vlera;
            }


            return shuma_A + shuma_B;

        }

        public static int firswithrest(int[] pitch)
        {

            List<int> bobi = new List<int>();
            int shuma_Alice = 0, rezBob = 0, shuma_Bob = 0;





            for (int i = 1; i < pitch.Length - 1; i++)
            {
                rezBob = Math.Abs(pitch[i + 1] - pitch[i]);


                bobi.Add(rezBob);


            }

            foreach (int vlera in bobi)
            {


                shuma_Bob += vlera;
            }




            return shuma_Alice + shuma_Bob;
        }

        public static int allwithlast(int[] pitch)
        {

            List<int> alice = new List<int>();
            int shuma_A = 0, rezA = 0, shuma_B = 0;

            for (int i = 0; i < pitch.Length - 2; i++)
            {
                rezA = Math.Abs(pitch[i + 1] - pitch[i]);
                alice.Add(rezA);


            }

            foreach (int vlera in alice)
            {
                shuma_A += vlera;
            }




            return shuma_A + shuma_B;
        }





        public static void solve(int getABBAA_unsorted, int getABBAA_sorted, int getHalf_unsorted, int getHalf_sorted, int firstwithrest_unsorted, int firstwithrest_sorted, int allwithlast_unsorted, int allwithlast_sorted)
        {

            int[] pitch = { getABBAA_unsorted, getHalf_unsorted, firstwithrest_unsorted, allwithlast_unsorted };
            int[] pitch1 = { getABBAA_sorted, getHalf_sorted, firstwithrest_sorted, allwithlast_sorted };

            Console.WriteLine("ABBAA Unsorted:" + getABBAA_unsorted + " \n");
            Console.WriteLine("ABBAA Sorted:" + getABBAA_sorted + " \n");
            Console.WriteLine("Get Half Unsorted:" + getHalf_unsorted + " \n");
            Console.WriteLine("Get Half Sorted:" + getHalf_sorted + " \n");
            Console.WriteLine("FirstWithRest Unsorted:" + firstwithrest_unsorted + " \n");
            Console.WriteLine("FirstWithRest Sorted:" + firstwithrest_sorted + " \n");
            Console.WriteLine("AllWithLast Unsorted:" + allwithlast_unsorted + " \n");
            Console.WriteLine("AllWithLast Sorted:" + allwithlast_sorted + " \n");
            Console.WriteLine("-------------------------------------");

            Console.WriteLine("\nGet Minimum Of Unsorted:" + pitch.Min());
            Console.WriteLine("\nGet Minimum Of Sorted:" + pitch1.Min() + " \n");



        }


        public static void getResult(int getHalf_unsorted, int getHalf_sorted, int firstwithrest_unsorted, int firstwithrest_sorted, int allwithlast_unsorted, int allwithlast_sorted)
        {

            int[] pitch = { getHalf_unsorted, firstwithrest_unsorted, allwithlast_unsorted };
            int[] pitch1 = { getHalf_sorted, firstwithrest_sorted, allwithlast_sorted };

            Console.WriteLine("Get Half Unsorted:" + getHalf_unsorted + " \n");
            Console.WriteLine("Get Half Sorted:" + getHalf_sorted + " \n");
            Console.WriteLine("FirstWithRest Unsorted:" + firstwithrest_unsorted + " \n");
            Console.WriteLine("FirstWithRest Sorted:" + firstwithrest_sorted + " \n");
            Console.WriteLine("AllWithLast Unsorted:" + allwithlast_unsorted + " \n");
            Console.WriteLine("AllWithLast Sorted:" + allwithlast_sorted + " \n");
            Console.WriteLine("-------------------------------------");

            Console.WriteLine("\nGet Minimum Of Unsorted:" + pitch.Min());
            Console.WriteLine("\nGet Minimum Of Sorted:" + pitch1.Min() + " \n");




        }


    }
}
